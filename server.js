// Hacer un require del módulo express, nuestro web framework por excelencia
var express = require('express');
// Construimos el objeto express
var app = new express();

var compression		= require('compression');
var bodyParser 		= require('body-parser');
var morgan 			= require('morgan');
var cookieParser 	= require('cookie-parser');

// Ocupamos el módulo express-session para gestionar sesiones
var session			= require('express-session');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(morgan('dev')); 		
app.use(compression());


// Configuramos express para que agregue un middleware de sessión
app.use(session({

	secret: "s14f8sd!",			// Generar encriptación
	saveUninitialized: 	false,	// No genera sesiones automáticamente
	resave: 			false 	 
}));

app.use(express.static(__dirname + '/public'));

var celulares = {

	RX78 : {

		precio: 2500,
		caracteristicas : {
			color : 'rojo',
			peso : '500gr',
			camara: 18,
			gps : true,
			ram : 2,
			cores : 4
		}
	},

	T23 : {

		precio: 7800,
		caracteristicas : {
			color : 'azul',
			peso : '700gr',
			camara: 15,
			gps : true,
			ram : 1,
			cores : 2
		}		
	}
};

var usuarios = {

	'dbaa@buap.mx': 'dbaa',
	'ruperto@buap.mx' : 'hola',
	'chespirito@televisa.mx' : 'elchavo'
};


// Rutas www
app.route('/')
	.get(function (request,response) {

	response.sendFile('/home/diegoaguilar/Git/nodejsdev/practicas/sesiones/html/index.html');
});

app.route('/publicar')
	.get(function (request,response) {

	response.sendFile('/home/diegoaguilar/Git/nodejsdev/practicas/sesiones/html/publicar.html');
});

app.route('/admin')

	.get(function (request,response) {
		response.sendFile('/home/diegoaguilar/Git/nodejsdev/practicas/sesiones/html/auth.html');	
	});



// Rutas autenticación

app.route('/signup/:username?')

	.get(function (request,response) {


	})

	.post(function (request,response) {

		var email = request.body.email;
		var password = request.body.password;

		usuarios[email] = password;
		request.session.email = email;
		console.log(usuarios);
		response.redirect('/admin');
	})

	.put(function (request,response) {


	});


app.route('/login')
	.post(function (request,response) {

		var email = request.body.email;
		var password = request.body.password;

		if (usuarios[email]) {

			if (usuarios[email] == password) {

				request.session.email = email;
				var loginTime = new Date();
				request.session.time = loginTime;
				response.cookie('loginTime',loginTime);
				response.status(202).end('Te conozco');
			}

			else response.status(401).send('No te conozco');
		}


		else {
			response.status(401).send('No te conozco');
		}
	});

app.route('/signout')
	.post(function (request,response) {

	});



// Rutas API

app.route('/celulares')
	.get(function (request,response) {

		console.log(request.cookies);
		if (request.session.email) {

			response.send({	'datos': celulares,
							'sesion': {'email':request.session.email,'time':request.session.time}
							});
		}
		else {
			response.status(401).send('Accede primero.');
		}
	})

	.post(function (request,response) {

		console.log(request.body);
		
		var id = request.body.id;
		var precio = request.body.precio;
		
		var nuevoCelular = {

			'precio' : precio
		};	
		
		celulares[id] = nuevoCelular;

		response.redirect('/celulares');
	});

app.route('/celulares/:id')
	.get(function (request,response) {



		var id = request.params.id;
		response.send(celulares[id]);
	});

app.route('/celulares/:id/:atributo')
	.get(function (request,response) {

		var id = request.params.id;
		var atributo = request.params.atributo;

		response.send(JSON.stringify(celulares[id][atributo]));
	});

// Ponemos a escuchar el servidor
app.listen(8080, function () {

	console.log('Escuchando');
});